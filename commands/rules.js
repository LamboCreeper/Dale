var config;
var Discord;
var client;

try {
	config = require('./../config.json');
	pkg = require('./../package.json');

	app = require('./../app.js');
	utils = require('./../utils.js');

	fs = require('fs');
	Discord = require('discord.js');
	client = new Discord.Client({fetchAllMembers: true});
} catch (e) {
	console.error(e);
}

exports.details = {
	command: "rules",
	prefix: true,
	visible: true,
	botchat: false,
	arguments: [],
	roles: []
};

exports.run = (message) => {
	message.channel.send(utils.mlm([
		"**Rules:**",
		"• No spamming",
		"• No racist / offensive words.",
		"• No earrape / music in voice chat.",
		"• Don't be a dick to new people.",
		"• Don't `@Matt` or `@TMGTom` - only tag `@Mods` if urgent.",
		"• No bad jokes.",
		"• No promoting other Discords / YT channels unless given permission.",
		"• No complaining about being banned in Twitch (you'll get banned here too).",
		"• No NSFW videos or pictures.",
		"• No selling any products or services.",
		"• No politics / religious debates.",
		"• Don't pretend someone else's setup is yours."
	]));
};
