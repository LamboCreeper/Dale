var config;
var Discord;
var client;
var request;

try {
	config = require('./../config.json');
	pkg = require('./../package.json');

	utils = require('./../utils.js');

	request = require('request');
	Discord = require('discord.js');
	client = new Discord.Client({fetchAllMembers: true});
} catch (e) {
	console.error(e);
}

exports.details = {
	command: "twitter",
	prefix: true,
	visible: true,
	botchat: true,
	arguments: [],
	roles: []
};

exports.run = (message) => {
	message.channel.send(utils.mlm([
		`**Twitter Accounts**`,
		"• URLs:",
		"  • <https://twitter.com/swashintv>",
		"  • <https://twitter.com/mattphilie>"
	]));
};
