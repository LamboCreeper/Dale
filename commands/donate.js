var config;
var Discord;
var client;

try {
	config = require('./../config.json');
	pkg = require('./../package.json');

	utils = require('./../utils.js');

	Discord = require('discord.js');
	client = new Discord.Client({fetchAllMembers: true});
} catch (e) {
	console.error(e);
}

exports.details = {
	command: "donate",
	prefix: true,
	visible: true,
	botchat: false,
	arguments: [],
	roles: []
};


exports.run = (message) => {
	const st = Math.floor(Math.random() * 5) + 1;
	message.channel.send(utils.mlm([
		`**Donating**`,
		"Whether you pledge one dollar a month or make a one-time donation, you are helping us create more content.",
		"Aside from paying the costs that come with the channel, your donations will be used for purchasing products and material for future content.",
		"\n",
		"You can either:",
		"• Join the swashin crew ($1/month).",
		"• Make a one time donation.",
		"\n",
		"Learn more here: <https://swashin.tv/support>."
	]));
};
