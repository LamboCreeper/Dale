var config;
var Discord;
var client;

try {
	config = require('./../config.json');
	pkg = require('./../package.json');

	utils = require('./../utils.js');

	Discord = require('discord.js');
	client = new Discord.Client({fetchAllMembers: true});
} catch (e) {
	console.error(e);
}

exports.details = {
	command: "help",
	prefix: true,
	visible: true,
	botchat: true,
	arguments: [],
	roles: []
};


exports.run = (message) => {
	const st = Math.floor(Math.random() * 5) + 1;
	message.channel.send(utils.mlm([
		`**Hey, ${message.author.username}!** :wave::skin-tone-${st}:`,
		"I'm Dale, the swashin Discord bot!",
		"\n",
		"For a list of commands: `!commands`",
		"\n",
		"**Boring Technical Stuff:**",
		`• Version: v${pkg.version}`,
		"• Source Code: <https://gitlab.com/LamboCreeper/Dale>",
		`• Uptime: ${utils.mtdc(client.uptime).clock}`
	]));
};
