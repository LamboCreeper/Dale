var config;
var Discord;
var client;

try {
	config = require('./../config.json');
	pkg = require('./../package.json');

	utils = require('./../utils.js');

	Discord = require('discord.js');
	client = new Discord.Client({fetchAllMembers: true});
} catch (e) {
	console.error(e);
}

exports.details = {
	command: "dale what do you rate my setup",
	prefix: false,
	visible: true,
	botchat: false,
	arguments: [],
	roles: []
};


exports.run = (message) => {
	const score = Math.floor(Math.random() * 100) + 1;
	const msgs = [
		{
			min: 0,
			responses: ["please just put your hand on one side of the table and slide it across."]
		},
		{
			min: 20,
			responses: ["HAVE YOU HEARD OF CABLE MANAGEMENT?"]
		},
		{
			min: 40,
			responses: ["ya nasty.", "\'cause that's a waste of space."]
		},
		{
			min: 60,
			responses: ["\'cause those bezels are fatter than your mom.", "\'cause that's wasted potential."]
		},
		{
			min: 80,
			responses: ["hey, that's pretty good."]
		}
	];
	for (var i = msgs.length - 1; i > 0; i--) {
		if (score >= msgs[i].min) {
			const ind = Math.floor(Math.random() * msgs[i].responses.length) + 0;
			msg = msgs[i].responses[ind];
			message.channel.send(`${score}/100 - ${msg}`);
			break;
		}
	}

};
