var config;
var Discord;
var client;

try {
	config = require('./../config.json');
	pkg = require('./../package.json');

	utils = require('./../utils.js');

	Discord = require('discord.js');
	client = new Discord.Client({fetchAllMembers: true});
} catch (e) {
	console.error(e);
}

exports.details = {
	command: "ping",
	prefix: true,
	visible: false,
	botchat: true,
	arguments: [],
	roles: []
};


exports.run = (message) => {
	message.channel.send(client.ping);
};
