var config;
var Discord;
var client;

try {
	config = require('./../config.json');
	pkg = require('./../package.json');

	app = require('./../app.js');
	utils = require('./../utils.js');

	fs = require('fs');
	Discord = require('discord.js');
	client = new Discord.Client({fetchAllMembers: true});
} catch (e) {
	console.error(e);
}

exports.details = {
	command: "commands",
	prefix: true,
	visible: true,
	botchat: true,
	arguments: [],
	roles: []
};

exports.run = (message) => {
	var msg = app.getCommands();
	msg.unshift("**Commands:**");
	message.channel.send(utils.mlm(msg));
};
