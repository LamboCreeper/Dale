var config;
var Discord;
var client;
var request;

try {
	config = require('./../config.json');
	pkg = require('./../package.json');

	utils = require('./../utils.js');

	request = require('request');
	Discord = require('discord.js');
	client = new Discord.Client({fetchAllMembers: true});
} catch (e) {
	console.error(e);
}

exports.details = {
	command: "youtube",
	prefix: true,
	visible: true,
	botchat: true,
	arguments: [],
	roles: []
};

exports.run = (message) => {
	console.log("Running!");
	request({
		method: "GET",
		url: `https://www.googleapis.com/youtube/v3/channels?part=statistics&id=UCRL9jNF5eKw2sdYPf6pCYjA&key=${config["youtube-key"]}`
	}, (err, res, body) => {
		if (err && res.statusCode != 200) {
			console.log("An error occurred with the YouTube API:");
			console.error(err);
		} else {
			body = JSON.parse(body);
			message.channel.send(utils.mlm([
				`**YouTube Channel Information**`,
				"• URL: <https://youtube.com/swashintv>",
				`• Subscribers: ${utils.commafy(body.items[0].statistics.subscriberCount)}`,
				`• Views: ${utils.commafy(body.items[0].statistics.viewCount)}`,
				`• Videos: ${utils.commafy(body.items[0].statistics.videoCount)}`
			]));
		}
	});
};
