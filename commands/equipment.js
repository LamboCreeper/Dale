var config;
var Discord;
var client;

try {
	config = require('./../config.json');
	pkg = require('./../package.json');

	utils = require('./../utils.js');

	Discord = require('discord.js');
	client = new Discord.Client({fetchAllMembers: true});
} catch (e) {
	console.error(e);
}

exports.details = {
	command: "equipment",
	prefix: true,
	visible: true,
	botchat: true,
	arguments: [],
	roles: []
};


exports.run = (message) => {
	message.channel.send(utils.mlm([
		`**Equipment List**`,
		"Microphones:",
		"• Matt: Shure SM7B",
		"• Tom: Shure SM58",
		"\n",
		"A full equipment list can be found on Kit: https://kit.com/swashin."
	]));
};
