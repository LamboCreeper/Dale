exports.mlm = (lines) => {
	var msg = "";
	for (line in lines) {
		msg += lines[line] + "\n";
		if (line == (lines.length - 1)) {
			return msg;
		}
	}
}

exports.mtdc = (ms) => {
    hours = Math.floor(ms / 3600000), // 1 Hour = 36000 Milliseconds
    minutes = Math.floor((ms % 3600000) / 60000), // 1 Minutes = 60000 Milliseconds
    seconds = Math.floor(((ms % 360000) % 60000) / 1000) // 1 Second = 1000 Milliseconds
    return {
        hours : hours,
        minutes : minutes,
        seconds : seconds,
        clock : hours + ":" + minutes + ":" + seconds
    };
}

exports.commafy = (num) => {
	// Shoutout to Elias Zamaria and Devin G Rhode on Stack Overflow for this!
	return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
