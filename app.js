var config;
var Discord;
var client;
var fs;

var commands = {};

try {
	config = require('./config.json');
	pkg = require('./package.json');

	fs = require('fs');
	Discord = require('discord.js');
	client = new Discord.Client({fetchAllMembers: true});

	fs.readdir("./commands/", (err, files) => {
		for (file in files) {
			commands[files[file].replace(".js", "")] = require(`./commands/${files[file]}`);
		}
	});
} catch (e) {
	console.error(e);
}

client.on("ready", () => {
	console.log(`Dale v${pkg.version} is now running!`);
});

client.on("message", (message) => {
	for (command in commands) {
		if (message.content.startsWith("!")) {
			if (commands[command].details.prefix) {
				var cmd = message.content.toLowerCase().replace("!", "");
				if (cmd == commands[command].details.command) {
					if ((commands[command].details.botchat) && (message.channel.id != "271722330848231426")) {
						message.delete();
						message.reply("Error: This command is only accessable within #bot-chat.").then((msg) => {
							msg.delete(10000);
						});
					} else {
						commands[command].run(message);
					}
				}
			}
		} else {
			if (!commands[command].details.prefix) {
				if (message.content.toLowerCase() == commands[command].details.command) {
					if ((commands[command].details.botchat) && (message.channel.id != "271722330848231426")) {
						message.delete();
						message.reply("Error: This command is only accessable within #bot-chat.").then((msg) => {
							msg.delete(10000);
						});
					} else {
						commands[command].run(message);
					}
				}
			}
		}
	}
});

client.on("guildMemberAdd", (member) => {
	const guild = client.guilds.find("id", "144996469433303040");
	const channel = guild.channels.find("id", "326367283108642817");
	channel.send(`Welcome to the swashin Discord, ${member}`);
});

client.on("guildMemberRemove", (member) => {
	const guild = client.guilds.find("id", "144996469433303040");
	const channel = guild.channels.find("id", "326367283108642817");
	channel.send(`Bye then, ${member}`);
});

exports.getCommands = () => {
	var cmds = [];
	var total = Object.keys(commands).length - 1;
	for (command in commands) {
		if (commands[command].details.visible) {
			cmds.push(`• \`${commands[command].details.prefix ? "!": ""}${commands[command].details.command}\``);
			if (cmds.length == total) {
				return cmds.sort();
			}
		} else {
			total--;
		}
	}
}

client.login(config.token);
